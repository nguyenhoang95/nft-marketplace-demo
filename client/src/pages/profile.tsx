import React, { useState, useEffect, useRef } from 'react';
import { Form, Formik, Field } from 'formik';
import { RepositoryFactory } from '@repositories/RepositoryFactory';
import { useAppSelector, useAppDispatch } from 'store';
import { fetchUser } from 'store/app';
import { toast } from 'react-toastify';
import { useCheckLogin } from '@hooks/use-check-login';

export const Profile = () => {
  const [profileImg, setProfileImg] = useState(
    'https://storage.googleapis.com/opensea-static/opensea-profile/18.png',
  );
  const [bannerImg, setBannerImg] = useState(
    'https://storage.googleapis.com/opensea-static/opensea-profile/18.png',
  );
  const profileImgInput = useRef(null);
  const bannerImgInput = useRef(null);
  const currentAddress = useAppSelector((state) => state.app.address);
  const user = useAppSelector((state) => state.app.user);
  const checkLogin = useCheckLogin();
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (user) {
      setProfileImg(user.profileImage);
      setBannerImg(user.profileBanner);
    }
  }, [user]);

  const initialValues = user
    ? {
        userName: user.userName,
        email: user.email,
        bio: user.bio,
        socialLink: user.socialLink,
      }
    : {};

  return (
    <div className="greyscheme">
      <section className="jumbotron breadcumb no-bg">
        <div className="mainbreadcumb">
          <div className="container">
            <div className="row m-10-hor">
              <div className="col-12">
                <h1 className="text-center">Profile</h1>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="container">
        <div className="row">
          <div className="col-lg-7 offset-lg-1 mb-5">
            {user && (
              <Formik
                initialValues={initialValues}
                onSubmit={async (data) => {
                  try {
                    await checkLogin();
                    await RepositoryFactory.get('auth').updateUser({
                      ...data,
                      profileBanner: bannerImg,
                      profileImage: profileImg,
                    });
                    dispatch(fetchUser(user.address));
                    toast.success('Profile Updated');
                  } catch (e) {
                    toast('Error occured');
                  }
                }}
              >
                <Form className="form-border">
                  <div className="field-set">
                    <h5>User name</h5>
                    <Field
                      type="text"
                      name="userName"
                      id="userName"
                      className="form-control"
                      placeholder="Enter username"
                    />

                    <div className="spacer-10"></div>

                    <h5>Bio</h5>

                    <Field
                      as="textarea"
                      name="bio"
                      id="bio"
                      className="form-control"
                      placeholder="Tell the world your story!"
                    />

                    <h5>Email</h5>

                    <Field
                      type="text"
                      name="email"
                      id="email"
                      className="form-control"
                      placeholder="Enter email"
                    />

                    <div className="spacer-10"></div>

                    <h5>Social</h5>

                    <Field
                      type="text"
                      name="socialLink"
                      id="socialLink"
                      className="form-control"
                      placeholder="Enter Social URLs like Instagram or Twitter"
                    />

                    <div className="spacer-10"></div>

                    <h5>Wallet Address</h5>
                    <input
                      type="text"
                      name="item_price"
                      id="item_price"
                      className="form-control"
                      value={currentAddress}
                      disabled
                    />

                    <div className="spacer-10"></div>

                    <button type="submit" className="btn-main">
                      Save
                    </button>
                  </div>
                </Form>
              </Formik>
            )}
          </div>

          <div className="col-lg-3 col-sm-6 col-xs-12">
            <h5>Profile image </h5>
            <img
              src={profileImg}
              id="click_profile_img"
              className="d-profile-img-edit img-fluid"
              alt=""
              style={{
                width: '150px',
                height: '150px',
                objectFit: 'cover',
                cursor: 'pointer',
              }}
              onClick={() => {
                (profileImgInput.current as any)?.click();
              }}
            />
            <input
              ref={profileImgInput}
              style={{ display: 'none' }}
              name="profile_image"
              type="file"
              id="upload_profile_img"
              accept="image/*"
              onChange={async (e) => {
                const file = e.target.files ? e.target.files[0] : undefined;
                if (file) {
                  e.target.value = '';
                  const url = await RepositoryFactory.get('fileUpload').uploadFile(file);
                  setProfileImg(url);
                }
              }}
            />

            <h5 className="mt-3">Profile banner </h5>
            <img
              src={bannerImg}
              id="click_banner_img"
              className="d-banner-img-edit img-fluid"
              style={{ cursor: 'pointer' }}
              onClick={() => {
                (bannerImgInput.current as any)?.click();
              }}
              alt=""
            />
            <input
              name="profile_banner"
              type="file"
              id="upload_banner_img"
              style={{ display: 'none' }}
              accept="image/*"
              ref={bannerImgInput}
              onChange={async (e) => {
                const file = e.target.files ? e.target.files[0] : undefined;
                if (file) {
                  e.target.value = '';
                  const url = await RepositoryFactory.get('fileUpload').uploadFile(file);
                  setBannerImg(url);
                }
              }}
            />
            <div className="spacer-30"></div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Profile;
