import type { AppProps } from 'next/app';
import { createGlobalStyle } from 'styled-components';
import Header from '@components/menu/Header';
import ScrollToTopBtn from '@components/menu/ScrollToTop';
import Footer from '@components/Footer';
import { ApolloProvider } from '@apollo/react-hooks';
import { initializeApollo } from '@lib/apollo-client';
import { ClientWrapper } from '@lib/client-wrapper';
import { ToastContainer, Slide } from 'react-toastify';

import '../../assets/animated.css';
import '../../node_modules/font-awesome/css/font-awesome.min.css';
import '../../node_modules/elegant-icons/style.css';
import '../../node_modules/et-line/style.css';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../../assets/style.scss';
import '../../assets/style_grey.scss';
import 'react-toastify/dist/ReactToastify.css';

import { Provider } from 'react-redux';
import { store } from 'store';

const GlobalStyles = createGlobalStyle`
  :root {
    scroll-behavior: unset;
  }
`;

function MyApp({ Component, pageProps }: AppProps) {
  const apolloClient = initializeApollo();

  return (
    <ApolloProvider client={apolloClient as any}>
      <Provider store={store}>
        <ClientWrapper>
          <GlobalStyles />
          <Header />
          <Component {...pageProps} />
          <ScrollToTopBtn />
          <Footer />
          <ToastContainer hideProgressBar autoClose={3000} transition={Slide} />
        </ClientWrapper>
      </Provider>
    </ApolloProvider>
  );
}

export default MyApp;
