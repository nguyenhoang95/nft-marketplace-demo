import React, { Component, useState } from 'react';
import { RepositoryFactory } from '@repositories/RepositoryFactory';
import { useAppSelector } from 'store';
import { toast } from 'react-toastify';
import { MetaMaskService } from '@core/nft/metamask';
import { useCheckLogin } from '@hooks/use-check-login';
import { useRouter } from 'next/router'

const Create = () => {
  const [previewUrl, setPreviewUrl] = useState('');
  const user = useAppSelector((state) => state.app.user);
  const [file, setFile] = useState<File>();
  const [title, setTitle] = useState('');
  const [des, setDes] = useState('');
  const checkLogin = useCheckLogin();
  const router = useRouter()

  const onSubmit = async () => {
    try {
      if (!previewUrl) {
        toast.warning('Image is required');
        return;
      }

      if (!title) {
        toast.warning('Title is required');
        return;
      }

      const data = await MetaMaskService.mintNFT(previewUrl, title, des);

      await checkLogin();

      const id = await RepositoryFactory.get('item').create({
        contractAddress: data.address,
        transactionHash: data.hash,
        fileUrl: previewUrl,
        tokenURI: data.tokenURI,
        title,
        description: des,
      });

      toast.success('Item Created');

      router.push(`/items/${id}`)
    } catch (e) {
      console.log(e);
      toast.error('Error occured');
    }
  };

  return (
    <div className="greyscheme">
      <section className="jumbotron breadcumb no-bg">
        <div className="mainbreadcumb">
          <div className="container">
            <div className="row m-10-hor">
              <div className="col-12">
                <h1 className="text-center">Create</h1>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="container">
        <div className="row">
          <div className="col-lg-7 offset-lg-1 mb-5">
            <form id="form-create-item" className="form-border" action="#">
              <div className="field-set">
                <h5>Upload file</h5>

                <div className="d-create-file">
                  <p id="file_name">{file?.name || 'PNG, JPG, GIF, WEBP. Max 3mb.'}</p>
                  <div className="browse">
                    <input type="button" id="get_file" className="btn-main" value="Browse" />
                    <input
                      id="upload_file"
                      type="file"
                      accept="image/*"
                      onChange={async (e) => {
                        const file = e.target.files ? e.target.files[0] : undefined;
                        if (file) {
                          setFile(file);
                          e.target.value = '';
                          const url = await RepositoryFactory.get('fileUpload').uploadFile(file);
                          setPreviewUrl(url);
                        }
                      }}
                    />
                  </div>
                </div>

                <div className="spacer-single"></div>

                <h5>Title</h5>
                <input
                  type="text"
                  name="item_title"
                  id="item_title"
                  className="form-control"
                  placeholder="e.g. 'Crypto Funk"
                  onChange={(e) => setTitle(e.target.value)}
                />

                <div className="spacer-10"></div>

                <h5>Desciption</h5>
                <textarea
                  data-autoresize
                  name="item_desc"
                  id="item_desc"
                  className="form-control"
                  placeholder="Tell the world your story!"
                  onChange={(e) => setDes(e.target.value)}
                ></textarea>

                <div className="spacer-10"></div>

                <input
                  type="button"
                  id="submit"
                  className="btn-main"
                  value="Create Item"
                  onClick={onSubmit}
                />
              </div>
            </form>
          </div>

          <div className="col-lg-3 col-sm-6 col-xs-12">
            <h5>Preview item</h5>
            <div className="nft__item m-0">
              <div className="author_list_pp">
                <span>
                  <img className="lazy" src={user?.profileImage} alt="" />
                  <i className="fa fa-check"></i>
                </span>
              </div>
              <div className="nft__item_wrap">
                <span>
                  <img
                    src={
                      previewUrl ||
                      'https://storage.googleapis.com/opensea-static/opensea-profile/18.png'
                    }
                    id="get_file_2"
                    className="lazy nft__item_preview"
                    alt=""
                  />
                </span>
              </div>
              <div className="nft__item_info">
                <span>
                  <h4>{title}</h4>
                </span>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Create;
