import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions = {} as const;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** The `JSON` scalar type represents JSON values as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf). */
  JSON: any;
};

export type Auth = {
  __typename?: 'Auth';
  token: Scalars['String'];
};

export type CreateAuthInput = {
  address: Scalars['String'];
  signature: Scalars['String'];
};

export type CreateItemInput = {
  contractAddress: Scalars['String'];
  description: Scalars['String'];
  fileUrl: Scalars['String'];
  title: Scalars['String'];
  tokenURI: Scalars['String'];
  transactionHash: Scalars['String'];
};

export type Item = {
  __typename?: 'Item';
  contractAddress: Scalars['String'];
  createdAt: Scalars['String'];
  description: Scalars['String'];
  fileUrl: Scalars['String'];
  id: Scalars['String'];
  owner: Scalars['String'];
  ownerDetail: User;
  price: Scalars['Float'];
  title: Scalars['String'];
  tokenId: Scalars['String'];
  tokenURI: Scalars['String'];
  transactionHash: Scalars['String'];
};

export type Mutation = {
  __typename?: 'Mutation';
  createAuth: Auth;
  createItem: Scalars['String'];
  getFileUploadToken: PresignedPostClass;
  logOut: Scalars['Boolean'];
  updateItem: Item;
  updateUser: Scalars['Boolean'];
};


export type MutationCreateAuthArgs = {
  createAuthInput: CreateAuthInput;
};


export type MutationCreateItemArgs = {
  input: CreateItemInput;
};


export type MutationGetFileUploadTokenArgs = {
  fileName: Scalars['String'];
};


export type MutationUpdateItemArgs = {
  updateItemInput: UpdateItemInput;
};


export type MutationUpdateUserArgs = {
  input: UpdateUserInput;
};

export type PresignedPostClass = {
  __typename?: 'PresignedPostClass';
  fields: Scalars['JSON'];
  url: Scalars['String'];
};

export type Query = {
  __typename?: 'Query';
  getNonce: Scalars['String'];
  item: Item;
  items: Array<Item>;
  user: User;
};


export type QueryGetNonceArgs = {
  address: Scalars['String'];
};


export type QueryItemArgs = {
  id: Scalars['String'];
};


export type QueryUserArgs = {
  address: Scalars['String'];
};

export type UpdateItemInput = {
  contractAddress?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  fileUrl?: InputMaybe<Scalars['String']>;
  id: Scalars['Int'];
  title?: InputMaybe<Scalars['String']>;
  tokenURI?: InputMaybe<Scalars['String']>;
  transactionHash?: InputMaybe<Scalars['String']>;
};

export type UpdateUserInput = {
  bio?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['String']>;
  profileBanner?: InputMaybe<Scalars['String']>;
  profileImage?: InputMaybe<Scalars['String']>;
  socialLink?: InputMaybe<Scalars['String']>;
  userName?: InputMaybe<Scalars['String']>;
};

export type User = {
  __typename?: 'User';
  /** Wallet address */
  address: Scalars['String'];
  bio: Scalars['String'];
  createdAt: Scalars['String'];
  email: Scalars['String'];
  loggedIn: Scalars['Boolean'];
  profileBanner: Scalars['String'];
  profileImage: Scalars['String'];
  socialLink: Scalars['String'];
  userName: Scalars['String'];
};

export type CreateAuthMutationVariables = Exact<{
  input: CreateAuthInput;
}>;


export type CreateAuthMutation = { __typename?: 'Mutation', createAuth: { __typename?: 'Auth', token: string } };

export type CreateItemMutationVariables = Exact<{
  input: CreateItemInput;
}>;


export type CreateItemMutation = { __typename?: 'Mutation', createItem: string };

export type CreateUploadTokenMutationVariables = Exact<{
  fileName: Scalars['String'];
}>;


export type CreateUploadTokenMutation = { __typename?: 'Mutation', getFileUploadToken: { __typename?: 'PresignedPostClass', url: string, fields: any } };

export type LogoutMutationVariables = Exact<{ [key: string]: never; }>;


export type LogoutMutation = { __typename?: 'Mutation', logOut: boolean };

export type UpdateUserMutationVariables = Exact<{
  input: UpdateUserInput;
}>;


export type UpdateUserMutation = { __typename?: 'Mutation', updateUser: boolean };

export type GetItemQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type GetItemQuery = { __typename?: 'Query', item: { __typename?: 'Item', id: string, fileUrl: string, owner: string, contractAddress: string, price: number, title: string, description: string, tokenURI: string, tokenId: string, transactionHash: string, ownerDetail: { __typename?: 'User', userName: string, profileImage: string } } };

export type GetNonceQueryVariables = Exact<{
  address: Scalars['String'];
}>;


export type GetNonceQuery = { __typename?: 'Query', getNonce: string };

export type GetUserQueryVariables = Exact<{
  address: Scalars['String'];
}>;


export type GetUserQuery = { __typename?: 'Query', user: { __typename?: 'User', address: string, createdAt: string, email: string, bio: string, socialLink: string, userName: string, profileImage: string, profileBanner: string, loggedIn: boolean } };


export const CreateAuthDocument = gql`
    mutation CreateAuth($input: CreateAuthInput!) {
  createAuth(createAuthInput: $input) {
    token
  }
}
    `;
export type CreateAuthMutationFn = Apollo.MutationFunction<CreateAuthMutation, CreateAuthMutationVariables>;

/**
 * __useCreateAuthMutation__
 *
 * To run a mutation, you first call `useCreateAuthMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateAuthMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createAuthMutation, { data, loading, error }] = useCreateAuthMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateAuthMutation(baseOptions?: Apollo.MutationHookOptions<CreateAuthMutation, CreateAuthMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateAuthMutation, CreateAuthMutationVariables>(CreateAuthDocument, options);
      }
export type CreateAuthMutationHookResult = ReturnType<typeof useCreateAuthMutation>;
export type CreateAuthMutationResult = Apollo.MutationResult<CreateAuthMutation>;
export type CreateAuthMutationOptions = Apollo.BaseMutationOptions<CreateAuthMutation, CreateAuthMutationVariables>;
export const CreateItemDocument = gql`
    mutation CreateItem($input: CreateItemInput!) {
  createItem(input: $input)
}
    `;
export type CreateItemMutationFn = Apollo.MutationFunction<CreateItemMutation, CreateItemMutationVariables>;

/**
 * __useCreateItemMutation__
 *
 * To run a mutation, you first call `useCreateItemMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateItemMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createItemMutation, { data, loading, error }] = useCreateItemMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateItemMutation(baseOptions?: Apollo.MutationHookOptions<CreateItemMutation, CreateItemMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateItemMutation, CreateItemMutationVariables>(CreateItemDocument, options);
      }
export type CreateItemMutationHookResult = ReturnType<typeof useCreateItemMutation>;
export type CreateItemMutationResult = Apollo.MutationResult<CreateItemMutation>;
export type CreateItemMutationOptions = Apollo.BaseMutationOptions<CreateItemMutation, CreateItemMutationVariables>;
export const CreateUploadTokenDocument = gql`
    mutation CreateUploadToken($fileName: String!) {
  getFileUploadToken(fileName: $fileName) {
    url
    fields
  }
}
    `;
export type CreateUploadTokenMutationFn = Apollo.MutationFunction<CreateUploadTokenMutation, CreateUploadTokenMutationVariables>;

/**
 * __useCreateUploadTokenMutation__
 *
 * To run a mutation, you first call `useCreateUploadTokenMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateUploadTokenMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createUploadTokenMutation, { data, loading, error }] = useCreateUploadTokenMutation({
 *   variables: {
 *      fileName: // value for 'fileName'
 *   },
 * });
 */
export function useCreateUploadTokenMutation(baseOptions?: Apollo.MutationHookOptions<CreateUploadTokenMutation, CreateUploadTokenMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateUploadTokenMutation, CreateUploadTokenMutationVariables>(CreateUploadTokenDocument, options);
      }
export type CreateUploadTokenMutationHookResult = ReturnType<typeof useCreateUploadTokenMutation>;
export type CreateUploadTokenMutationResult = Apollo.MutationResult<CreateUploadTokenMutation>;
export type CreateUploadTokenMutationOptions = Apollo.BaseMutationOptions<CreateUploadTokenMutation, CreateUploadTokenMutationVariables>;
export const LogoutDocument = gql`
    mutation Logout {
  logOut
}
    `;
export type LogoutMutationFn = Apollo.MutationFunction<LogoutMutation, LogoutMutationVariables>;

/**
 * __useLogoutMutation__
 *
 * To run a mutation, you first call `useLogoutMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLogoutMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [logoutMutation, { data, loading, error }] = useLogoutMutation({
 *   variables: {
 *   },
 * });
 */
export function useLogoutMutation(baseOptions?: Apollo.MutationHookOptions<LogoutMutation, LogoutMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<LogoutMutation, LogoutMutationVariables>(LogoutDocument, options);
      }
export type LogoutMutationHookResult = ReturnType<typeof useLogoutMutation>;
export type LogoutMutationResult = Apollo.MutationResult<LogoutMutation>;
export type LogoutMutationOptions = Apollo.BaseMutationOptions<LogoutMutation, LogoutMutationVariables>;
export const UpdateUserDocument = gql`
    mutation UpdateUser($input: UpdateUserInput!) {
  updateUser(input: $input)
}
    `;
export type UpdateUserMutationFn = Apollo.MutationFunction<UpdateUserMutation, UpdateUserMutationVariables>;

/**
 * __useUpdateUserMutation__
 *
 * To run a mutation, you first call `useUpdateUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateUserMutation, { data, loading, error }] = useUpdateUserMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateUserMutation(baseOptions?: Apollo.MutationHookOptions<UpdateUserMutation, UpdateUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateUserMutation, UpdateUserMutationVariables>(UpdateUserDocument, options);
      }
export type UpdateUserMutationHookResult = ReturnType<typeof useUpdateUserMutation>;
export type UpdateUserMutationResult = Apollo.MutationResult<UpdateUserMutation>;
export type UpdateUserMutationOptions = Apollo.BaseMutationOptions<UpdateUserMutation, UpdateUserMutationVariables>;
export const GetItemDocument = gql`
    query GetItem($id: String!) {
  item(id: $id) {
    id
    fileUrl
    owner
    contractAddress
    price
    fileUrl
    title
    description
    tokenURI
    tokenId
    transactionHash
    ownerDetail {
      userName
      profileImage
    }
  }
}
    `;

/**
 * __useGetItemQuery__
 *
 * To run a query within a React component, call `useGetItemQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetItemQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetItemQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetItemQuery(baseOptions: Apollo.QueryHookOptions<GetItemQuery, GetItemQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetItemQuery, GetItemQueryVariables>(GetItemDocument, options);
      }
export function useGetItemLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetItemQuery, GetItemQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetItemQuery, GetItemQueryVariables>(GetItemDocument, options);
        }
export type GetItemQueryHookResult = ReturnType<typeof useGetItemQuery>;
export type GetItemLazyQueryHookResult = ReturnType<typeof useGetItemLazyQuery>;
export type GetItemQueryResult = Apollo.QueryResult<GetItemQuery, GetItemQueryVariables>;
export const GetNonceDocument = gql`
    query GetNonce($address: String!) {
  getNonce(address: $address)
}
    `;

/**
 * __useGetNonceQuery__
 *
 * To run a query within a React component, call `useGetNonceQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetNonceQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetNonceQuery({
 *   variables: {
 *      address: // value for 'address'
 *   },
 * });
 */
export function useGetNonceQuery(baseOptions: Apollo.QueryHookOptions<GetNonceQuery, GetNonceQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetNonceQuery, GetNonceQueryVariables>(GetNonceDocument, options);
      }
export function useGetNonceLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetNonceQuery, GetNonceQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetNonceQuery, GetNonceQueryVariables>(GetNonceDocument, options);
        }
export type GetNonceQueryHookResult = ReturnType<typeof useGetNonceQuery>;
export type GetNonceLazyQueryHookResult = ReturnType<typeof useGetNonceLazyQuery>;
export type GetNonceQueryResult = Apollo.QueryResult<GetNonceQuery, GetNonceQueryVariables>;
export const GetUserDocument = gql`
    query GetUser($address: String!) {
  user(address: $address) {
    address
    createdAt
    email
    bio
    socialLink
    userName
    profileImage
    profileBanner
    loggedIn
  }
}
    `;

/**
 * __useGetUserQuery__
 *
 * To run a query within a React component, call `useGetUserQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUserQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetUserQuery({
 *   variables: {
 *      address: // value for 'address'
 *   },
 * });
 */
export function useGetUserQuery(baseOptions: Apollo.QueryHookOptions<GetUserQuery, GetUserQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetUserQuery, GetUserQueryVariables>(GetUserDocument, options);
      }
export function useGetUserLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetUserQuery, GetUserQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetUserQuery, GetUserQueryVariables>(GetUserDocument, options);
        }
export type GetUserQueryHookResult = ReturnType<typeof useGetUserQuery>;
export type GetUserLazyQueryHookResult = ReturnType<typeof useGetUserLazyQuery>;
export type GetUserQueryResult = Apollo.QueryResult<GetUserQuery, GetUserQueryVariables>;