import {
  GetNonceQuery,
  GetNonceQueryVariables,
  CreateAuthMutation,
  CreateAuthMutationVariables,
  CreateAuthInput,
  GetUserQuery,
  GetUserQueryVariables,
  UpdateUserMutation,
  UpdateUserMutationVariables,
  UpdateUserInput,
  User,
  LogoutMutation,
  LogoutMutationVariables,
} from '@generated/graphql';
import getNonceQuery from '@graphql/queries/nonce.graphql';
import getUserQuery from '@graphql/queries/user.graphql';
import creatAuthMutation from '@graphql/mutations/create-auth.graphql';
import updateUserMutation from '@graphql/mutations/update-user.graphql';
import logoutMutation from '@graphql/mutations/logout.graphql';
import Repository from './Repository';

export default {
  async getNonce(address: string): Promise<string> {
    const { data } = await Repository.query<GetNonceQuery, GetNonceQueryVariables>({
      query: getNonceQuery,
      variables: {
        address,
      },
    });

    return data.getNonce;
  },

  async getuser(address: string): Promise<User> {
    const { data } = await Repository.query<GetUserQuery, GetUserQueryVariables>({
      query: getUserQuery,
      variables: {
        address,
      },
    });

    return data.user as User;
  },

  async createAuth(input: CreateAuthInput) {
    const { data } = await Repository.mutation<CreateAuthMutation, CreateAuthMutationVariables>({
      mutation: creatAuthMutation,
      variables: {
        input,
      },
    });
  },

  async updateUser(input: UpdateUserInput) {
    const { data } = await Repository.mutation<UpdateUserMutation, UpdateUserMutationVariables>({
      mutation: updateUserMutation,
      variables: {
        input,
      },
    });
  },

  async logout() {
    const { data } = await Repository.mutation<LogoutMutation, LogoutMutationVariables>({
      mutation: logoutMutation,
    });
  },
};
