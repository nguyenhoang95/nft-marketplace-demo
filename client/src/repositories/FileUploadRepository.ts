import {
  CreateUploadTokenMutation,
  CreateUploadTokenMutationVariables,
  PresignedPostClass,
} from '@generated/graphql';
import createUploadTokenMutation from '@graphql/mutations/create-upload-token.graphql';
import Repository from './Repository';
import axios from 'axios';

const FileUploadRepo = {
  async createUploadToken(fileName: string): Promise<PresignedPostClass> {
    const { data } = await Repository.query<
      CreateUploadTokenMutation,
      CreateUploadTokenMutationVariables
    >({
      query: createUploadTokenMutation,
      variables: {
        fileName,
      },
    });

    return data.getFileUploadToken as PresignedPostClass;
  },

  async uploadFile(file: File): Promise<string> {
    const token = await FileUploadRepo.createUploadToken(file.name);
    const form = new FormData();

    Object.entries(token.fields).forEach(([field, value]) => {
      form.append(field, value as string);
    });

    form.append('file', file);
    await axios.post(token.url, form);

    return `${token.url}/${token.fields.key}`;
  },
};

export default FileUploadRepo;
