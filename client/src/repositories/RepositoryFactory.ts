import AuthRepository from './AuthRepository';
import FileUploadRepository from './FileUploadRepository';
import ItemRepository from './ItemRepository';

const repositories = {
  auth: AuthRepository,
  fileUpload: FileUploadRepository,
  item: ItemRepository,
};

export const RepositoryFactory = {
  get<K extends keyof typeof repositories>(name: K) {
    return repositories[name];
  },
};
