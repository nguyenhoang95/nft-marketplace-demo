import {
  CreateItemMutation,
  CreateItemMutationVariables,
  CreateItemInput,
  GetItemQuery,
  GetItemQueryVariables,
  Item,
} from '@generated/graphql';
import createItemnMutation from '@graphql/mutations/create-item.graphql';
import getItemQuery from '@graphql/queries/item.graphql';
import Repository from './Repository';

const ItemRepo = {
  async create(input: CreateItemInput): Promise<string> {
    const { data } = await Repository.query<CreateItemMutation, CreateItemMutationVariables>({
      query: createItemnMutation,
      variables: {
        input,
      },
    });

    return data.createItem;
  },

  async getById(id: string): Promise<Item> {
    const { data } = await Repository.query<GetItemQuery, GetItemQueryVariables>({
      query: getItemQuery,
      variables: {
        id,
      },
    });

    return data.item as Item;
  },
};

export default ItemRepo;
