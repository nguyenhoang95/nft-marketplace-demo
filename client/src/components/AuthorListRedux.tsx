import React, { memo, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import UserTopSeller from './UserTopSeller';

const AuthorList = () => {
  const dispatch = useDispatch();

  return (
    <div>
      <ol className="author_list">
        {[].map((author, index) => (
          <li key={index}>
            <UserTopSeller user={author} />
          </li>
        ))}
      </ol>
    </div>
  );
};
export default memo(AuthorList);
