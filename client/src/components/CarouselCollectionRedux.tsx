import React, { memo, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { settings } from './constants';
import CustomSlide from './CustomSlide';
import api from '@core/api';

const CarouselCollectionRedux = () => {
  const dispatch = useDispatch();

  return (
    <div className="nft">
      <Slider {...settings}>
        {[].map((item, index) => (
          <CustomSlide
            key={index}
            index={index + 1}
            avatar={api.baseUrl + item.author.avatar.url}
            banner={api.baseUrl + item.banner.url}
            username={item.name}
            uniqueId={item.unique_id}
            collectionId={item.id}
          />
        ))}
      </Slider>
    </div>
  );
};

export default memo(CarouselCollectionRedux);
