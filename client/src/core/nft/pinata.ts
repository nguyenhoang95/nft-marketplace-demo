const key = '1e31d9f2f249e5f0576f';
const secret = 'a00dbac7a6dbf6b8b4c6908389f12aeef71ee373e4401c03ca46e6882bd4b35e';
import axios from 'axios';

export const pinJSONToIPFS = async (JSONBody: any) => {
  const url = `https://api.pinata.cloud/pinning/pinJSONToIPFS`;
  return axios
    .post(url, JSONBody, {
      headers: {
        pinata_api_key: key,
        pinata_secret_api_key: secret,
      },
    })
    .then(function (response: any) {
      return {
        success: true,
        pinataUrl: 'https://gateway.pinata.cloud/ipfs/' + response.data.IpfsHash,
      };
    })
    .catch(function (error: any) {
      console.log(error);
      return {
        success: false,
        message: error.message,
        pinataUrl: '',
      };
    });
};
