import { MetaMaskInpageProvider } from '@metamask/providers';
import { RepositoryFactory } from '@repositories/RepositoryFactory';
import { pinJSONToIPFS } from './pinata';
import contractABI from './contract-abi.json';
import { ethers } from 'ethers';
import { toast } from 'react-toastify';
import { number } from 'yup';
import marketABI from './market-abi.json'
import { Item } from '@generated/graphql'

const CONTRACT_ADDRESS = '0xcDaE69df30F8cE469FA6D16363083F693087CbFA';
const MARKET_ADDRESS = '0xc3640FA6B141cD7a9Ae822Bea6Ad9AA18845B356'

function utf8ToHex(str: string) {
  const buf = Buffer.from(str, 'utf8');
  return buf.toString('hex');
}

declare global {
  interface Window {
    ethereum: MetaMaskInpageProvider;
    contract: any;
  }
}

class MetaMask {
  provider: ethers.providers.Web3Provider;

  constructor() {
    this.provider =
      typeof window === 'undefined'
        ? ({} as ethers.providers.Web3Provider)
        : new ethers.providers.Web3Provider(window.ethereum as any);
  }

  isInstalled(): boolean {
    return Boolean(window.ethereum);
  }
  async connectWallet() {
    if (!this.isInstalled()) {
      return null;
    }

    try {
      await this.provider.send('eth_requestAccounts', []);
      toast.success('Wallet connected');
      localStorage.removeItem('wallet_disconnected');
    } catch (e) {
      console.log(e);
    }
  }

  async signIn() {
    const address = await this.getCurrentConnectedAccount();
    if (address) {
      const nonce = await RepositoryFactory.get('auth').getNonce(address);

      const data = await window.ethereum.request({
        method: 'personal_sign',
        params: [utf8ToHex(nonce), address],
      });

      await RepositoryFactory.get('auth').createAuth({
        address,
        signature: data as string,
      });
    }
  }

  async disconnect() {}

  async listItem(item: Item, price: number) {
    console.log('start list item')
    const signer = this.provider.getSigner();
    const contract = new ethers.Contract(MARKET_ADDRESS, marketABI.abi, signer);
    // const data = await contract.addItemToMarket(CONTRACT_ADDRESS, 1, 1)
    const data = await contract.getUnsoldItems()
    console.log(data)
  }

  async mintNFT(url: string, name: string, description: string) {
    const metadata: any = {};
    metadata.name = name;
    metadata.image = url;
    metadata.description = description;

    const pinataResponse = await pinJSONToIPFS(metadata);
    const tokenURI = pinataResponse.pinataUrl;
    const signer = this.provider.getSigner();
    const contract = new ethers.Contract(CONTRACT_ADDRESS, contractABI.abi, signer);
    const address = await this.getCurrentConnectedAccount()

    const data = await contract.mintNFT(address, tokenURI);

    return {
      hash: data.hash as string,
      address: CONTRACT_ADDRESS,
      tokenURI,
    };
  }

  async getCurrentConnectedAccount() {
    const addresses: string[] = await this.provider.send('eth_accounts', []);

    return addresses ? addresses[0] : undefined;
  }
}

export const MetaMaskService = new MetaMask();
