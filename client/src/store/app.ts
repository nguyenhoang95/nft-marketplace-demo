import { createSlice, PayloadAction, createAsyncThunk } from '@reduxjs/toolkit';
import { User } from '@generated/graphql';
import { RepositoryFactory } from '@repositories/RepositoryFactory';

export interface AppState {
  user?: User;
  address: string;
}

const initialState: AppState = {
  address: '',
};

export const fetchUser = createAsyncThunk('app/fetchUser', RepositoryFactory.get('auth').getuser);

const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    setAddress: (state, action: PayloadAction<string>) => {
      state.address = action.payload;
      return state;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchUser.fulfilled, (state, { payload }) => {
      state.user = payload;
      return state;
    });
  },
});

export const { setAddress } = appSlice.actions;

export default appSlice.reducer;
