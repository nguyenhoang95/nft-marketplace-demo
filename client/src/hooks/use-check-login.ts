import { useAppDispatch, useAppSelector } from 'store';
import { fetchUser } from 'store/app';
import { MetaMaskService } from '@core/nft/metamask';

export const useCheckLogin = () => {
  const user = useAppSelector((state) => state.app.user);
  const dispatch = useAppDispatch();

  return async () => {
    if (!user?.loggedIn) {
      await MetaMaskService.signIn();
      dispatch(fetchUser(user?.address || ''));
    }
  };
};
