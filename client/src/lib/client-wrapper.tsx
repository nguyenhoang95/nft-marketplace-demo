import { fetchUser, setAddress } from 'store/app';
import { useAppDispatch, useAppSelector } from 'store';
import { useEffect } from 'react';
import { StyledHeader } from '@styled/styles';
import { MetaMaskService } from '@core/nft/metamask';

interface Props {}

export const ClientWrapper: React.FC<Props> = ({ children }) => {
  const distpach = useAppDispatch();
  const address = useAppSelector((state) => state.app.address);
  const theme = address ? 'GREYLOGIN' : 'GREY';

  useEffect(() => {
    if (address) {
      distpach(fetchUser(address));
    }
  }, [address]);

  useEffect(() => {
    const fetchAddress = async () => {
      const data = await MetaMaskService.getCurrentConnectedAccount();
      if (data) {
        distpach(setAddress(data));
      }
    };

    if (!localStorage.getItem('wallet_disconnected')) {
      fetchAddress();
    }
  }, []);

  return (
    <>
      <StyledHeader theme={theme} />
      {children}
    </>
  );
};
