import { ObjectType, Field, HideField } from '@nestjs/graphql';
import { Entity, PrimaryColumn, CreateDateColumn, Column } from 'typeorm';

@Entity()
@ObjectType()
export class User {
  @PrimaryColumn()
  @Field(() => String, { description: 'Wallet address' })
  address: string;

  @CreateDateColumn()
  @Field(() => String)
  createdAt: string;

  @Column()
  @HideField()
  nonce: number;

  @Column()
  email: string;
  
  @Column()
  bio: string;
  
  @Column()
  socialLink: string;
  
  @Column()
  userName: string;
  
  @Column()
  profileImage: string;
  
  @Column()
  profileBanner: string;

  loggedIn: boolean;
}
