import { InputType, Field, Int } from '@nestjs/graphql';

@InputType()
export class UpdateUserInput {
  email?: string;

  bio?: string;

  socialLink?: string;

  userName?: string;

  profileImage?: string;

  profileBanner?: string;
}
