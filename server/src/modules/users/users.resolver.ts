import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { UsersService } from './users.service';
import { User } from './entities/user.entity';
import { UpdateUserInput } from './dto/update-user.input';
import { GqlAuthGuard, GqlAuthGuardOptional } from '@modules/auth/gql-auth.guard';
import { CurrentUser } from '@utils/current-user';

@Resolver(() => User)
export class UsersResolver {
  constructor(private readonly usersService: UsersService) {}
  
  @UseGuards(GqlAuthGuardOptional)
  @Query(() => User, { name: 'user' })
  async findOne(
    @Args('address', { type: () => String }) address: string,
    @CurrentUser() user: User
  ) {
    const data = await this.usersService.findOne(address);
    data.loggedIn = !!user

    return data
  }
  
  @UseGuards(GqlAuthGuard)
  @Mutation(() => Boolean)
  async updateUser(@CurrentUser() user: User,  @Args('input') updateUserInput: UpdateUserInput) {
    await this.usersService.update(user.address, updateUserInput);
    return true
  }
}
