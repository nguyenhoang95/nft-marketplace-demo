import { Injectable } from '@nestjs/common';
import { CreateUserInput } from './dto/create-user.input';
import { UpdateUserInput } from './dto/update-user.input';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  create(createUserInput: CreateUserInput) {
    return this.usersRepository.insert(createUserInput);
  }

  findAll() {
    return `This action returns all users`;
  }

  findOne(address: string) {
    return this.usersRepository.findOne(address);
  }

  updateNonce(address: string, nonce: number) {
    return this.usersRepository.update(address, { nonce });
  }

  update(address: string, input: UpdateUserInput) {
    return this.usersRepository.update(address, input);
  }

  remove(id: number) {
    return `This action removes a #${id} user`;
  }
}
