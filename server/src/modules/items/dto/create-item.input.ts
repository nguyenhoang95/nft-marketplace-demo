import { InputType, Int, Field, HideField } from '@nestjs/graphql';

@InputType()
export class CreateItemInput {

  @HideField()
  owner: string;
  
  @Field()
  contractAddress: string;

  @Field()
  fileUrl: string;

  @Field()
  transactionHash: string;

  @Field()
  tokenURI: string;

  @Field()
  title: string;

  @Field()
  description: string;
}
