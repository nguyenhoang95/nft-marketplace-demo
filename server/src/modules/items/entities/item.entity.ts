import { ObjectType, Field, Float } from '@nestjs/graphql';
import { Entity, PrimaryColumn, CreateDateColumn, Column } from 'typeorm';
import { User } from '@modules/users/entities/user.entity'

@Entity()
@ObjectType()
export class Item {
   
  @PrimaryColumn()
  id: string;
  
  @Column()
  owner: string;
  
  @Column()
  contractAddress: string;

  @CreateDateColumn()
  createdAt: string;
  
  @Field(() => Float)
  @Column({ default: 0 })
  price: number

  @Column()
  fileUrl: string;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column()
  transactionHash: string;

  @Column({ default: '' })
  tokenId: string;

  @Column()
  tokenURI: string;

  @Field(() => User)
  ownerDetail: User;
}
