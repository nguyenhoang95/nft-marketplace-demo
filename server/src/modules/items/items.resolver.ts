import { Resolver, Query, Mutation, Args, Int, ResolveField, Parent } from '@nestjs/graphql';
import { ItemsService } from './items.service';
import { Item } from './entities/item.entity';
import { CreateItemInput } from './dto/create-item.input';
import { UpdateItemInput } from './dto/update-item.input';
import { UseGuards } from '@nestjs/common';
import { GqlAuthGuard } from '@modules/auth/gql-auth.guard';
import { CurrentUser } from '@utils/current-user';
import { User } from '@modules/users/entities/user.entity';
import { UsersService } from '@modules/users/users.service'

@Resolver(() => Item)
export class ItemsResolver {
  constructor(private readonly itemsService: ItemsService, private userService: UsersService) {}

  @UseGuards(GqlAuthGuard)
  @Mutation(() => String)
  createItem(@Args('input') input: CreateItemInput, @CurrentUser() user: User) {
    return this.itemsService.create({
      ...input,
      owner: user.address
    });
  }

  @Query(() => [Item], { name: 'items' })
  findAll() {
    return this.itemsService.findAll();
  }

  @Query(() => Item, { name: 'item' })
  findOne(@Args('id', { type: () => String }) id: string) {
    return this.itemsService.findOne(id);
  }

  @Mutation(() => Item)
  updateItem(@Args('updateItemInput') updateItemInput: UpdateItemInput) {
    return this.itemsService.update(updateItemInput.id, updateItemInput);
  }

  @ResolveField('ownerDetail', () => User)
  async getOwner(@Parent() item: Item) {
    return this.userService.findOne(item.owner);
  }
}
