import { Module } from '@nestjs/common';
import { ItemsService } from './items.service';
import { ItemsResolver } from './items.resolver';
import { Item } from './entities/item.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '@modules/users/users.module'

@Module({
  imports: [TypeOrmModule.forFeature([Item]), UsersModule],
  providers: [ItemsResolver, ItemsService]
})
export class ItemsModule {}
