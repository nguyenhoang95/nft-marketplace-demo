import { Injectable } from '@nestjs/common';
import { CreateItemInput } from './dto/create-item.input';
import { UpdateItemInput } from './dto/update-item.input';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Item } from './entities/item.entity'
import { v4 } from 'uuid'

@Injectable()
export class ItemsService {
  constructor(
    @InjectRepository(Item)
    private itemRepository: Repository<Item>,
  ) {}

  async create(input: CreateItemInput) {
    const id = v4()
    await this.itemRepository.insert({
      id,
      ...input
    })

    return id
  }

  findAll() {
    return `This action returns all items`;
  }

  findOne(id: string) {
    return this.itemRepository.findOne(id);
  }

  update(id: number, updateItemInput: UpdateItemInput) {
    return `This action updates a #${id} item`;
  }

  remove(id: number) {
    return `This action removes a #${id} item`;
  }
}
