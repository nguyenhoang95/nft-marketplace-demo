import { Resolver, Mutation, Args } from '@nestjs/graphql';
import { FileUploadService } from './file-upload.service';
import { createPresignedPost, PresignedPost } from '@aws-sdk/s3-presigned-post';
import { S3Client, Condition } from '@aws-sdk/client-s3';
import { v4 } from 'uuid';
import { PresignedPostClass } from './entities/file-upload.entity'

const s3Client = new S3Client({
  region: 'ap-southeast-1',
});


@Resolver(() => PresignedPostClass)
export class FileUploadResolver {
  constructor(private readonly fileUploadService: FileUploadService) {}

  @Mutation(() => PresignedPostClass)
  async getFileUploadToken(@Args('fileName', { type: () => String }) fileName: string): Promise<PresignedPostClass> {
    const id = v4()
    const extention = fileName.split('.').reverse()[0]
    const Conditions = [
      { acl: 'public-read' },
      { bucket: 'nft-demo-hamsa' },
      ['starts-with', '$key', id],
      ['content-length-range', 0, 30 * 1048576],
    ] as Condition[];

    const Fields = {
      acl: 'public-read',
    };

    const signed = await createPresignedPost(s3Client, {
      Bucket: 'nft-demo-hamsa',
      Key: `${id}.${extention}`,
      Conditions,
      Expires: 600,
      Fields
    });

    return signed

  }
}
