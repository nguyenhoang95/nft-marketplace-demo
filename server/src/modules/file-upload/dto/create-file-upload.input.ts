import { InputType, Int, Field } from '@nestjs/graphql';

@InputType()
export class CreateFileUploadInput {
  @Field(() => Int, { description: 'Example field (placeholder)' })
  exampleField: number;
}
