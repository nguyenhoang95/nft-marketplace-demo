import { CreateFileUploadInput } from './create-file-upload.input';
import { InputType, Field, Int, PartialType } from '@nestjs/graphql';

@InputType()
export class UpdateFileUploadInput extends PartialType(CreateFileUploadInput) {
  @Field(() => Int)
  id: number;
}
