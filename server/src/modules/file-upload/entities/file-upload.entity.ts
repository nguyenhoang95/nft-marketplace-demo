import { ObjectType, Field, Int } from '@nestjs/graphql';
import { PresignedPost } from '@aws-sdk/s3-presigned-post';
import GraphQLJSON from 'graphql-type-json';

@ObjectType()
export class PresignedPostClass implements PresignedPost {

  @Field()
  url: string;

  @Field(() => GraphQLJSON)
  fields: { [key: string]: string; };
}
