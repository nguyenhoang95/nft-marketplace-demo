import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class CreateAuthInput {
  @Field(() => String)
  address: string;

  @Field(() => String)
  signature: string;
}
