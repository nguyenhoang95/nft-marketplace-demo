import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { CreateAuthInput } from './dto/create-auth.input';
import { UsersService } from '@modules/users/users.service';
import { User } from '@modules/users/entities/user.entity';
import { recoverPersonalSignature } from '@metamask/eth-sig-util';
import { JwtService } from '@nestjs/jwt';
import { Payload } from './constants';

@Injectable()
export class AuthService {
  constructor(
    private userService: UsersService,
    private jwtService: JwtService,
  ) {}

  async create(createAuthInput: CreateAuthInput) {
    const user = await this.userService.findOne(createAuthInput.address);

    const recoveredAddress = recoverPersonalSignature({
      data: this.getAuthMessage(user.nonce),
      signature: createAuthInput.signature,
    });

    if (recoveredAddress === user.address) {
      const token = this.createToken(user);
      await this.userService.updateNonce(user.address, this.createNonce());

      return {
        token,
      };
    } else {
      throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
    }
  }

  createNonce() {
    return Math.floor(Math.random() * 1000000);
  }

  getAuthMessage(nonce: number) {
    return `
   Well come to my first dApp

   Nonce: ${nonce}
 `;
  }

  createToken(user: User) {
    const payload: Payload = { address: user.address, sub: user.address };

    return this.jwtService.sign(payload);
  }

  async getNonce(address: string): Promise<string> {
    const user = await this.userService.findOne(address);

    if (user) {
      return this.getAuthMessage(user.nonce);
    }

    const nonce = this.createNonce();
    await this.userService.create({
      address,
      nonce,
    });

    return this.getAuthMessage(user.nonce);
  }
}
