export const jwtConstants = {
  secret: 'secretKey',
};

export interface Payload {
  address: string;
  sub: string;
}
