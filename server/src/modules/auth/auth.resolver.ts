import { Resolver, Query, Mutation, Args, Context } from '@nestjs/graphql';
import { AuthService } from './auth.service';
import { Auth } from './entities/auth.entity';
import { CreateAuthInput } from './dto/create-auth.input';

@Resolver(() => Auth)
export class AuthResolver {
  constructor(private readonly authService: AuthService) {}

  @Mutation(() => Auth)
  async createAuth(
    @Args('createAuthInput') createAuthInput: CreateAuthInput,
    @Context() context: any,
  ): Promise<Auth> {
    const auth = await this.authService.create(createAuthInput);
    const cookieHeader = `access_token=${auth.token}; Path=/; Max-Age=3600; HttpOnly`;
    context.res.setHeader('set-cookie', cookieHeader);

    return auth;
  }

  @Mutation(() => Boolean)
  async logOut(
    @Context() context: any,
  ): Promise<boolean> {
    const cookieHeader = `access_token=; Path=/; Max-Age=3600; HttpOnly`;
    context.res.setHeader('set-cookie', cookieHeader);
    return true;
  }

  @Query(() => String)
  getNonce(
    @Args('address', { type: () => String }) address: string,
  ): Promise<string> {
    return this.authService.getNonce(address);
  }
}
