async function main() {
    const MarketPlace = await ethers.getContractFactory("NFTMarket")
  
    // Start deployment, returning a promise that resolves to a contract object
    const market = await MarketPlace.deploy()
    await market.deployed()
    console.log("Contract deployed to address:", market.address)
  }

  main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });